package session2;//// change the package name to your package name
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.MarionetteDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateLeadFirefox {

	public static void main(String[] args) throws InterruptedException {

FirefoxDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("http://encreo.com/crm");
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("crmuser");
driver.findElement(By.xpath("//input[@name='password']")).sendKeys("crmuser");
		driver.findElement(By.xpath("//button[text()='Sign in']")).click();
		driver.findElement(By.xpath("(//strong[text()='Leads'])[1]")).click();
		driver.findElement(By.xpath("//strong[text()='Add Lead']")).click();
		driver.findElement(By.xpath("//input[@name='firstname']")).sendKeys("Sanjay");
		driver.findElement(By.xpath("//input[@id='Leads_editView_fieldName_lastname']")).sendKeys("Sharma");
		driver.findElement(By.xpath("//strong[text()='Save']")).click();
		String fullName=driver.findElement(By.xpath("//h4")).getText();
		System.out.println("Full name ::"+ fullName);
		driver.findElement(By.xpath("//strong[text()='Demo']")).click();
		// tell driver to wait for the element to be visible within the timeout.
		WebDriverWait waiter = new WebDriverWait(driver, 20);
		waiter.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='Sign Out']")));
		driver.findElement(By.xpath("//a[text()='Sign Out']")).click();
		driver.close();
		driver.quit(); 
		
	}

}
