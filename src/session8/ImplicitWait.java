package session8;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class ImplicitWait {
  
	@Test
	public void implicitWait() {
		System.setProperty("webdriver.chrome.driver", 
				"/home/bc/bc/_work/_binaryzombies/_ws/WebdriverProject/drivers/chromedriver");
		
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.get("http://binaryzombies.com/vtiger/index.php");
		
		driver.findElement(By.xpath("//input[@type='text'][@placeholder='agent vikram']")).click();
	}
	
	@Test
	 public void withoutImplicitWait() {
		
		System.setProperty("webdriver.chrome.driver", 
				"/home/bc/bc/_work/_binaryzombies/_ws/WebdriverProject/drivers/chromedriver");
		WebDriver driver = new ChromeDriver();
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.get("http://binaryzombies.com/vtiger/index.php");

		driver.findElement(By.xpath("//input[@type='text'][@placeholder='agent vikram']")).click();

	  }
}

