package session8;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.MarionetteDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;

public class ExplicitWaits2 {
	
	
  @Test
  public void waitForElementToBeClickable() throws Exception {
	  
	 
		System.setProperty("webdriver.chrome.driver", 
				"./drivers/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.get("http://binaryzombies.com/vtiger/index.php");
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("crmuser");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("crmuser");
		driver.findElement(By.xpath("//button[.='Sign in']")).click();
		
		driver.findElement(By.xpath("(//strong[.='Leads'])[1]/..")).click();
		
		driver.findElement(By.xpath("//input[@name='firstname']")).sendKeys("bha");
		driver.findElement(By.xpath("//button[.='Search']")).click();
		
		WebDriverWait wait = new WebDriverWait(driver, 20); // pass the driver object, with timeout in seconds
		
		//wait.until(ExpectedConditions.alertIsPresent()); // wait for an alert to be present
		
		//wait for element to be visible
		//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[.='bharath']"))));
		
		//wait for element to be clickable
		//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//a[.='bharath']"))));
		
		//wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[.='Loading, Please wait.........']")));
		//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//a[.='bharath']"))));
		
		//wait.until(ExpectedConditions.stalenessOf(driver.findElement(By.xpath("//*[.='Loading, Please wait.........']"))));
/*		Actions actions = new Actions(driver);
		actions.moveToElement(driver.findElement(By.xpath("//a[.='bharath']")));*/

		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='blockUI blockOverlay']")));
		
		driver.findElement(By.xpath("//a[.='bharath']")).click();


		//org.openqa.selenium.WebDriverException: unknown error: Element is not clickable

		System.out.println("Click is done.");
		driver.quit();
	  
  }
  

  @Test
  public void waitInFirefox(){

	  
		 
		System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.get("http://binaryzombies.com/vtiger/index.php");
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("crmuser");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("crmuser");
		driver.findElement(By.xpath("//button[.='Sign in']")).click();
		
		JavascriptExecutor jsRunner = (JavascriptExecutor)driver;
		WebElement leadsLink =driver.findElement(By.xpath("(//strong[.='Leads'])[1]/.."));
		 Object readyState = jsRunner.executeScript("return document.readyState;");
		 System.out.println("ready state returned by js ::"+ readyState.toString());
		jsRunner.executeScript("arguments[0].click;", leadsLink);
		driver.findElement(By.xpath("(//strong[.='Leads'])[1]/..")).click();
		
		driver.findElement(By.xpath("//input[@name='firstname']")).sendKeys("bha");
		driver.findElement(By.xpath("//button[.='Search']")).click();
		
		WebDriverWait wait = new WebDriverWait(driver, 20); // pass the driver object, with timeout in seconds
		
		//wait.until(ExpectedConditions.alertIsPresent()); // wait for an alert to be present
		
		//wait for element to be visible
		//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[.='bharath']"))));
		
		//wait for element to be clickable
		//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//a[.='bharath']"))));
		
		//wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[.='Loading, Please wait.........']")));
		//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//a[.='bharath']"))));
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='blockUI blockOverlay']")));
		
		driver.findElement(By.xpath("//a[.='bharath']")).click();


		System.out.println("Click is done.");
		driver.quit();
	  

	  
  }
}


