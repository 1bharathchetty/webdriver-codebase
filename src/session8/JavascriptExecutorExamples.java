package session8;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.MarionetteDriver;
import org.testng.annotations.Test;


public class JavascriptExecutorExamples {
	
	@Test
  public void javascriptTests() {
	  
		System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.get("http://binaryzombies.com/vtiger/index.php");

		
		JavascriptExecutor jsRunner = (JavascriptExecutor)driver; // casting driver from webdriver to JSE
		
		String loginpageTitle = (String) jsRunner.executeScript("return document.title;");
		System.out.println("loginpageTitle:: "+ loginpageTitle);
		
		//driver.findElement(By.xpath("//input[@name='username']")).sendKeys("crmuser");
		jsRunner.executeScript("document.getElementById('username').value='crmuser';" );
		
		//driver.findElement(By.xpath("//input[@name='password']")).sendKeys("crmuser");
		jsRunner.executeScript("document.getElementById('password').value='crmuser';" );
		
		//driver.findElement(By.xpath("//button[.='Sign in']")).click();

		//jsRunner.executeScript("document.getElementsByTagName('button').click;");
		
		String usernameEntered = (String) jsRunner.executeScript("return document.getElementById('username').value;");
		System.out.println("Username entered ::"+ usernameEntered);
		
		WebElement signinButton = driver.findElement(By.xpath("//button[.='Sign in']"));
		
		jsRunner.executeScript("arguments[0].click();", signinButton);
		
		String pageloadState = (String) jsRunner.executeScript("return document.readyState;");
		System.out.println("pageloadState  ::"+ pageloadState);
		
		String homepageTitle = (String) jsRunner.executeScript("return document.title;");
		System.out.println("homepageTitle  ::"+ homepageTitle);
		
  }
}


