package session4;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Test;

public class AssertionsExamples {

	@Test
	public void compareStringsTest() {
		//Assert.assertEquals(expected, actual)
		assertEquals(" Strings should be equal" , "BHARATH", "BHARATH") ;
	}
	
	
	@Test
	public void compareNumbersTest() {
		assertEquals("Numbers should be equal" , 100, 99);
	}
	
	
	@Test
	public void compareBooleanTest() {
		assertEquals("Boolean values should be equal" , true, false  );
	}

	
	@Test
	public void compareUsingNOTTest() {
		assertNotSame("NUmbers should not be same" , 100, 1001 );
	}

}


