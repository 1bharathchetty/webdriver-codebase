package session4;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BeforeAfterExamplesTest {

	@Before
	public void start() throws Exception {
		System.out.println("run this before each test");
	}

	@After
	public void finish() throws Exception {
		System.out.println("clean up after each test");

	}

	@Test
	public void testone() {
		System.out.println("your test one is running");
	}
	
	@Test
	public void testtwo() {
		System.out.println("your test two is running");
	}
	@Test
	public void testthree() {
		System.out.println("your test three is running");
	}

}


