package session1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class LaunchFirefox {

	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver", "drivers/geckodriver");
		WebDriver firefox = new FirefoxDriver();
		firefox.get("http://redpawns.com");
		System.out.println(firefox.findElements(By.tagName("a")).size());

	}

}


