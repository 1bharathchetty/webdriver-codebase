package session6;

import static org.junit.Assert.*;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ThreeWindows {
	
	WebDriver driver;

	@Test
	public void test() {
		
		System.setProperty("webdriver.chrome.driver" ,"./drivers/chromedriver");
		driver = new ChromeDriver(); 
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.get("http://encreo.com/in/resources.html");
		
		String xp_SignInLink ="//a[.='Sign in']";
		driver.findElement(By.xpath(xp_SignInLink)).click();
		String xp_PostLink ="//*[contains(text(), 'A chrome extension for Selenium Users.')]";
		driver.findElement(By.xpath(xp_PostLink)).click();
		
		Set<String> winHandles = driver.getWindowHandles();
		int windowsPresent = driver.getWindowHandles().size();
		System.out.println("Window count ::"+ windowsPresent);
		
		String  firstWindowTitle = "Resources | Encreo Test Studio ";
		String  secondWindowTitle = "Encreo Test Studio";
		String  thirdWindowTitle = "Encreo UI Spy | Encreo Test Studio ";
		
		
		switchToWindowWithTitle(firstWindowTitle);
		System.out.println("Switched to ..."+ driver.getCurrentUrl());
		switchToWindowWithTitle(secondWindowTitle);		
		System.out.println("Switched to ..."+ driver.getCurrentUrl());

		switchToWindowWithTitle(thirdWindowTitle);
		System.out.println("Switched to ..."+ driver.getCurrentUrl());

		switchToWindowWithTitle(secondWindowTitle);
		System.out.println("Switched to ..."+ driver.getCurrentUrl());

		switchToWindowWithTitle(firstWindowTitle);		
		System.out.println("Switched to ..."+ driver.getCurrentUrl());


		switchToWindowByCount(1);
		System.out.println("Switched to ..."+ driver.getCurrentUrl());

		switchToWindowByCount(2);
		System.out.println("Switched to ..."+ driver.getCurrentUrl());
		
		switchToWindowByCount(1);
		System.out.println("Switched to ..."+ driver.getCurrentUrl());
		
		switchToWindowByCount(3);
		System.out.println("Switched to ..."+ driver.getCurrentUrl());
		
/*		for(String winHandle : winHandles){
			System.out.println(winHandle);
			driver.switchTo().window(winHandle);
			System.out.println("switched to window with handle ::"+ winHandle);
			String currentWindowTitle = driver.getTitle();
			System.out.println("Window title ::"+ currentWindowTitle);
			System.out.println("Window current URL  ::"+ driver.getCurrentUrl());

			String expectedWindowHandle = thirdWindowTitle;
			
			if(currentWindowTitle.equals(expectedWindowHandle)){
			
				break;
			}
		}
		System.out.println("CUrrent URL ::"+ driver.getCurrentUrl());*/
	}
	
	
	public void switchToWindowWithTitle(String windowTitle){
		Set<String> winHandles = driver.getWindowHandles();
		for(String winHandle : winHandles){
			System.out.println(winHandle);
			driver.switchTo().window(winHandle);
			System.out.println("switched to window with handle ::"+ winHandle);
			String currentWindowTitle = driver.getTitle();
			System.out.println("Window title ::"+ currentWindowTitle);
			System.out.println("Window current URL  ::"+ driver.getCurrentUrl());

			String expectedWindowHandle = windowTitle;
			
			if(currentWindowTitle.equals(expectedWindowHandle)){
			
				break;
			}
		}
		System.out.println("CUrrent URL ::"+ driver.getCurrentUrl());
	}
	
	
	
	public void switchToWindowByCount(int windowCount){
		Set<String> winHandles = driver.getWindowHandles();
		int counter=1;
		for(String winHandle : winHandles){
			System.out.println(winHandle);
			driver.switchTo().window(winHandle);
			System.out.println("switched to window with handle ::"+ winHandle);
			String currentWindowTitle = driver.getTitle();
			System.out.println("Window title ::"+ currentWindowTitle);
			System.out.println("Window current URL  ::"+ driver.getCurrentUrl());

			if(counter==windowCount){
			
				break;
			}
			counter++;
		}
		System.out.println("CUrrent URL ::"+ driver.getCurrentUrl());
	}

}


